import requests
from pydantic import BaseModel, ValidationError, Field
from typing import List

apiresponse = requests.get('http://api.openweathermap.org/data/2.5/weather?q=Zhukovsky\
                            &appid=665132a6ffb8aa3c1aace3b2c5f49b57')


class Coordinates(BaseModel):
    lon: float
    lat: float


class WeatherInfo(BaseModel):
    id_weather: int = Field(alias="id")
    mainWeather: str = Field(alias="main")
    description: str
    icon: str


class MainInformation(BaseModel):
    temp: float
    feels_like: str
    temp_min: float
    temp_max: float
    pressure: int
    humidity: int


class WindInformation(BaseModel):
    speed: int
    deg: int


class CloudsInformation(BaseModel):
    all: int


class SystemInformation(BaseModel):
    type_info: int = Field(alias="type")
    id_info: int = Field(alias="id")
    country: str
    sunrise: int
    sunset: int


class APIInfo(BaseModel):
    coord: Coordinates
    weather: List[WeatherInfo]
    base: str
    mainInfo: MainInformation = Field(alias="main")
    visibility: int
    wind: WindInformation
    clouds: CloudsInformation
    dt: int
    sys: SystemInformation
    timezone: int
    name: str
    cod: int


try:
    info = APIInfo.parse_raw(apiresponse.content)
except ValidationError as e:
    print("Exception", e.json())
else:
    print("city: ", info.name, ", ", info.sys.country, "\n",
          round(info.mainInfo.temp-273.15, 1), "C\n", info.weather[0].description, "\n",      #K->C
          info.wind.speed, "m/s, wind\n", info.mainInfo.humidity, "%, humidity\n",
          round(info.mainInfo.pressure*100/133.3224), "mmHg, pressure\n")                     #hPa->mmHg
